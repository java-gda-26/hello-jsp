Zadanie 1
---------
Napisz aplikację wyświetającą napis "Hello, JSP!" w przeglądarce internetowej

Zadanie 2
---------
Rozszerz aplikację o nagłówek i stopkę które zdefiniowane będą w oddzielnych plikach JSP oraz poszczególne podstrony.

Nagłówek powinien zawierać nazwę aplikacji (np. Hello, JSP!) oraz menu składające się z następujących pozycji:
  - 'Main page' kierująca do pliku index.jsp
  - 'Uptime' kierująca do pliku uptime.jsp
  - 'Random number' kierująca do pliku number.jsp
  - 'Multiplication table' kierująca do pliku multiplication.jsp

Stopka powinna zawierać imię i nazwisko autora aplikacji

Zadanie 3 - uptime.jsp
----------------------
W pliku uptime.jsp wyświetl czas działania aplikacji (czas od jej uruchomienia) w formacie XXh XXm (np. 2h 34m)

Zadanie 4 - number.jsp
----------------------
Wylosuj liczbę całkowitą z przedziału 0-99 i wypisz ją na ekranie.

Jeśli liczba jest mniejsza od 50 wyświetl na ekranie napis "Liczba mniejsza od 50", a samą liczbę pokoloruj na niebiesko
Jeśli liczba jest równa lub większa 50 wyświetl na ekranie napisz "Liczba równa lub większa od 50", a samą liczbę pokoloruj na zielono

Zadanie 5 - multiplication.jsp
------------------------------
Wypisz na ekranie tabliczkę mnożenia. Zastosuj 3 różne style kolorowania tła dla każdego wyniku w tabliczce mnożenia. Pierwszy styl zastosuj dla przekątnej od lewego górnego rogu do prawego dolnego. Drugi styl dla górnego trójkąta, trzeci styl dla dolnego.

Zadanie 6 - multiplication.jsp
------------------------------
Dodaj do aplikacji możliwość wyświetlenia tabliczki mnożenia o dowolnym wymiarze pionowym i poziomym

Zadanie 7 - multipication.jsp
-----------------------------
Dane wpisane w formularzu z wymiarami tabliczki mnożenia powinny zostać zapamiętane w sesji lub w ciasteczkach

Zadanie 8 - dynamiczne menu
---------------------------
Zaznacz w menu pogrubioną czcionką/kolorem tła obecnie otwartą stronę aplikacji

Zadanie 9 - json.jsp
--------------------
Utwórz klasę Person zawierającą pola zawierające następujące dane:
  - imię
  - nazwisko
  - adres e-mail
  - telefon
  
Utwórz instancję tej klasy wypełniając ją przykładowymi danymi i zwróć w formacie json.
