<%@ page import="java.util.Random" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hello, JSP!</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<jsp:include page="menu.jsp">
    <jsp:param name="position" value="3"/>
</jsp:include>
<h1>Random number</h1>

<%
    Random r = new Random();
    int randomNumber = r.nextInt(100);

    String color = "blue";
    if (randomNumber < 50) {
        out.print("<br>Wylosowana liczba jest mniejsza od 50<br>");
    } else {
        out.print("<br>Wylosowana liczba jest większa od 50<br>");
        color = "red";
    }

    String formattedNumber = String.format("Random: <span style='color: %s'>%d</span>", color, randomNumber);
    out.print(formattedNumber);
%>

<%@include file="footer.jsp" %>
</body>
</html>