<%@ page import="pl.sdacademy.pl.sdacademy.Person"%>
<%@ page import="com.google.gson.Gson"%>
<%@ page contentType="application/json;charset=UTF-8" language="java" %>
<%
Person person = new Person();
person.setFirstName("Jan");
person.setLastName("Kowalski");
person.setEmail("jan@kowalski.pl");
person.setPhoneNumber("+48123456789");

Gson gson = new Gson();
String json = gson.toJson(person);
out.print(json);
%>