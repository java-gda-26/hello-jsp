<a href="index.jsp" class="${param.position eq 1 ? 'active' : ''}">Hello, JSP!</a> |
<a href="uptime.jsp" class="${param.position eq 2 ? 'active' : ''}">Uptime</a> |
<a href="random.jsp" class="${param.position eq 3 ? 'active' : ''}">Random number</a> |
<a href="multiplication.jsp" class="${param.position eq 4 ? 'active' : ''}">Multiplication table</a> |
Hello, ${empty sessionScope.firstName ?  "JSP!" : sessionScope.firstName}