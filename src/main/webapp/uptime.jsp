<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hello, JSP!</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<jsp:include page="menu.jsp">
    <jsp:param name="position" value="2"/>
</jsp:include>
<h1>Uptime</h1>
<%! private long startTime = System.currentTimeMillis(); %>

<%!
    private String getUptimeMessage() {
        long upTime = System.currentTimeMillis() - startTime;
        long hours = upTime / 3600000;
        long minutes = upTime / 60000 % 60;
        long seconds = upTime / 1000 % 60;

        return String.format("Uptime: %dh %dm %ds", hours, minutes, seconds);
    }
%>

<%= getUptimeMessage()%>

<%@include file="footer.jsp" %>
</body>
</html>