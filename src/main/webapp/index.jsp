<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Hello, JSP!</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<jsp:include page="menu.jsp">
    <jsp:param name="position" value="1"/>
</jsp:include>

<c:if test="${not empty param.firstName}">
    <c:set var="firstName" value="${param.firstName}" scope="session"/>
</c:if>

<%--Sprawdzamy czy w sesji istnieje atrybut (zmienna) o nazwie "firstName".--%>
<%--Jeśli istnieje, używamy jej do powitania użytkownika--%>
<h1>Hello, <%= session.getAttribute("firstName") != null ? session.getAttribute("firstName") : "JSP"%>!</h1>

<form action="index.jsp" method="post">
    <label for="firstNameInputId">Imię</label>
    <input type="text" id="firstNameInputId" name="firstName"/>
    <input type="submit" value="Wyślij"/>
</form>

<%@include file="footer.jsp" %>
</body>
</html>