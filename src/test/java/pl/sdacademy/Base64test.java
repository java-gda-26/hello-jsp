package pl.sdacademy;

import org.junit.Test;

import java.util.Base64;

public class Base64test {

    @Test
    public void base64test() {
        String string = "Jan Kowalski";
        System.out.println("Plaint text: " + string);

        byte[] base64byteArray = Base64.getEncoder().encode(string.getBytes());
        String base64String = new String(base64byteArray);
        System.out.println("Base64: " + base64String);

        byte[] decodedByteArray = Base64.getDecoder().decode(base64String);
        String decodedString = new String(decodedByteArray);
        System.out.println("Decoded text: " + decodedString);
    }
}
