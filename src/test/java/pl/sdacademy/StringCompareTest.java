package pl.sdacademy;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class StringCompareTest {

    @Test
    public void compare1test() {
        // given
        String string1 = "1";
        String string2 = "parameter";

        // when
        boolean result = string1.equals(string2);

        // then
        assertThat(result).isFalse();
    }

    @Test(expected = NullPointerException.class)
    public void compare2test() {
        // given
        String string1 = null;
        String string2 = "parameter";

        // when
        boolean result = string1.equals(string2);

        // then
    }
}
